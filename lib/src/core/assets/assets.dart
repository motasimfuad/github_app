class Assets {
  static const String _root = 'assets';
  static const String _icons = '$_root/icons';

  /// Logo
  static const String logo = '$_icons/logo.png';
}
