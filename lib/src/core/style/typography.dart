import 'package:flutter/material.dart';

class TextStyles {
  TextStyle medium16 = const TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );

  TextStyle light16 = const TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w300,
  );
}
