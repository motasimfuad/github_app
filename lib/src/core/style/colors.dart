import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFF583176);
  static const Color secondary = Color(0xFFFBEDEA);

  static const Color black = Color(0xFF000000);
  static const Color black10 = Color(0xFFF3F4FA);
  static Color black50 = const Color(0xFF000000).withOpacity(.50);
  static Color black60 = const Color(0xFF2B2B2B).withOpacity(.60);
  static Color black75 = const Color(0xFF000000).withOpacity(.75);
  static Color black80 = const Color(0xFF2B2B2B).withOpacity(.80);
  static Color black90 = const Color(0xFF2B2B2B).withOpacity(.90);

  static const Color lightGrey = Color(0xFFD6D6D6);
  static const Color darkGrey = Color(0xFF565656);
  static const Color greyBackground = Color(0xFFCDDDE7);

  static const Color white = Color(0xFFFFFFFF);
  static Color white50 = const Color(0xFFFFFFFF).withOpacity(.50);
  static Color white75 = const Color(0xFFFFFFFF).withOpacity(.75);

  static const Color primaryText = Color(0xFF272727);
  static const Color secondaryText = Color(0xFF989898);

  static const Color loader = Color(0xFFF3F4FA);
  static const Color background = Color(0xFF24292F);
  static const Color transparent = Color(0x00000000);
}
