import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Button extends StatelessWidget {
  const Button({
    Key? key,
    required this.label,
    required this.onPressed,
    this.background,
    this.textStyle,
    this.scale = 1,
    this.height = 55,
    this.width = double.infinity,
    this.borderRadius = 50,
    this.isLoading = false,
    this.filled = true,
    this.prefix,
    this.disable = false,
  }) : super(key: key);

  final VoidCallback onPressed;
  final String label;
  final Color? background;
  final TextStyle? textStyle;
  final double scale;
  final double height;
  final double width;
  final double borderRadius;
  final bool filled;
  final bool isLoading;
  final Widget? prefix;
  final bool disable;

  @override
  Widget build(BuildContext context) {
    double height = this.height;

    return DecoratedBox(
      decoration: disable || filled == false
          ? const BoxDecoration()
          : BoxDecoration(
              gradient: const LinearGradient(
                colors: [
                  Color(0xFF0381E0),
                  Color(0xFF045689),
                ],
                begin: Alignment.centerRight,
                end: Alignment.centerLeft,
              ),
              borderRadius: BorderRadius.circular(borderRadius),
            ),
      child: ElevatedButton(
        onPressed: disable
            ? null
            : isLoading
                ? null
                : onPressed,
        style: filled
            ? ElevatedButton.styleFrom(
                elevation: 0,
                fixedSize: Size(width, height),
                backgroundColor: background ?? Colors.transparent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(borderRadius),
                ),
              )
            : ElevatedButton.styleFrom(
                shadowColor: Colors.transparent,
                elevation: 0,
                fixedSize: Size(width, height),
                backgroundColor:
                    disable ? Colors.grey : background ?? Colors.transparent,
                side: BorderSide(
                  color: isLoading ? Colors.transparent : Colors.blue,
                  width: 1,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(borderRadius),
                ),
              ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            isLoading
                ? Transform.scale(
                    scale: scale,
                    child: const SizedBox(
                      height: 30,
                      width: 30,
                      child: CircularProgressIndicator(),
                    ),
                  )
                : prefix == null
                    ? Text(
                        label,
                        style: textStyle ?? _labelTextStyle(),
                      )
                    : Row(
                        children: [
                          prefix!,
                          SizedBox(width: 10.w),
                          Text(
                            label,
                            style: textStyle ?? _labelTextStyle(),
                          ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }

  TextStyle _labelTextStyle() {
    return TextStyle(
      color: filled ? Colors.white : Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.w600,
    );
  }
}
