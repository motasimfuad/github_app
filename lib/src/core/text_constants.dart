class TextConstants {
  static const String defaultErrorMessage = 'Something went wrong';
  static const String couldNotConnectToServer =
      'Could not connect to server. Please try again later.';
  static const String requestCancelled =
      'Request has been cancelled. Please try again later.';
}
