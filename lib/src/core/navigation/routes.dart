class Routes {
  static const String splash = '/';

  /// Main
  static const String home = '/home';
  static const String repoDetails = '/repoDetails';
}
