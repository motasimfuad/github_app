enum BaseStatus {
  /// The initial state of the widget.
  initial,

  /// The state of the widget when it is loading.
  loading,

  /// The state of the widget when it is loaded.
  success,

  /// The state of the widget when it is empty.
  empty,

  /// The state of the widget when it is error.
  error,
}

class BaseState<T> {
  BaseState({
    this.status = BaseStatus.initial,
    this.message,
    this.response,
    this.request,
  });

  factory BaseState.initial() => BaseState();

  factory BaseState.success({T? response}) {
    return BaseState(
      status: BaseStatus.success,
      response: response,
      message: null,
      request: null,
    );
  }

  final BaseStatus status;
  final String? message;
  final T? response;
  final T? request;

  BaseState<T> copyWith({
    BaseStatus? status,
    String? message,
    T? response,
    T? request,
  }) {
    return BaseState<T>(
      status: status ?? this.status,
      message: message ?? this.message,
      response: response ?? this.response,
      request: request ?? this.request,
    );
  }
}
