class CacheKeys {
  static const String isOnBoardingDone = 'isOnBoardingDone';
  static const String isUserLoggedIn = 'isUserLoggedIn';
  static const String token = 'token';
  static const String refreshToken = 'refreshToken';
  static const String firebaseToken = 'firebaseToken';
  static const String isSubscribed = 'isSubscribed';
  static const String user = 'user';
}
