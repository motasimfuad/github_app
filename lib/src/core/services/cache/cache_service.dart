import 'package:github_app/src/core/services/cache/cache_service_impl.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final cacheServiceProvider = Provider<CacheService>(
  (ref) => CacheServiceImpl(),
);

abstract class CacheService {
  Future<void> init();

  Future<void> save(String key, dynamic value);

  Future<dynamic> read(String key);

  Future<void> delete(String key);

  Future<void> deleteAll();
}
