import 'error_model.dart';

class NetworkException implements Exception {
  NetworkException(this.errorModel);

  final ErrorResponseModel errorModel;

  @override
  String toString() {
    return errorModel.toString();
  }
}

class NotFoundException extends NetworkException {
  NotFoundException(ErrorResponseModel errorModel) : super(errorModel);
}

class RequestTimeoutException extends NetworkException {
  RequestTimeoutException(ErrorResponseModel errorModel) : super(errorModel);
}

class BadRequestException extends NetworkException {
  BadRequestException(ErrorResponseModel errorModel) : super(errorModel);
}

class BadGatewayException extends NetworkException {
  BadGatewayException(ErrorResponseModel errorModel) : super(errorModel);
}

class DefaultException extends NetworkException {
  DefaultException(ErrorResponseModel errorModel) : super(errorModel);
}

class RequestCancelledException extends NetworkException {
  RequestCancelledException(ErrorResponseModel errorModel) : super(errorModel);
}

class ReceiveTimeoutException extends NetworkException {
  ReceiveTimeoutException(ErrorResponseModel errorModel) : super(errorModel);
}

class FetchDataException extends NetworkException {
  FetchDataException(ErrorResponseModel errorModel) : super(errorModel);
}

class UnauthorisedException extends NetworkException {
  UnauthorisedException(ErrorResponseModel errorModel) : super(errorModel);
}

class ConflictException extends NetworkException {
  ConflictException(ErrorResponseModel errorModel) : super(errorModel);
}

class InternalServerException extends NetworkException {
  InternalServerException(ErrorResponseModel errorModel) : super(errorModel);
}

class UnexpectedException extends NetworkException {
  UnexpectedException(ErrorResponseModel errorModel) : super(errorModel);
}

class InvalidInputException extends NetworkException {
  InvalidInputException(ErrorResponseModel errorModel) : super(errorModel);
}

class UnAuthenticatedException extends NetworkException {
  UnAuthenticatedException(ErrorResponseModel errorModel) : super(errorModel);
}

class BadCertificateException extends NetworkException {
  BadCertificateException(ErrorResponseModel errorModel) : super(errorModel);
}

class BadResponseException extends NetworkException {
  BadResponseException(ErrorResponseModel errorModel) : super(errorModel);
}
