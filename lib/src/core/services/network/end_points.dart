class API {
  static const String prod = 'https://api.github.com';
  static const String dev = 'https://api.github.com';

  static const base = dev;

  /// Repositories
  static const repositories = '/search/repositories';
}
