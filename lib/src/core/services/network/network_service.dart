import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_app/src/core/logger.dart';
import 'package:github_app/src/core/services/cache/cache_keys.dart';
import 'package:github_app/src/core/services/cache/cache_service.dart';
import 'package:github_app/src/core/services/exception/error_model.dart';
import 'package:github_app/src/core/services/exception/network_exception.dart';
import 'package:github_app/src/core/text_constants.dart';

import 'end_points.dart';
import 'pretty_dio_logger.dart';

final networkServiceProvider = Provider((ref) => NetworkService(
      cacheService: ref.read(cacheServiceProvider),
    ));

class NetworkService {
  late Dio _dio;

  NetworkService({
    required this.cacheService,
  }) {
    BaseOptions options = BaseOptions(
      baseUrl: API.base,
      connectTimeout: const Duration(seconds: 30),
      receiveTimeout: const Duration(seconds: 30),
    );
    _dio = Dio(options);
  }

  final CacheService cacheService;

  Future<Response<dynamic>> get(
    APIType apiType,
    String path, {
    Map<String, dynamic>? queries,
    Map<String, dynamic>? headers,
  }) async {
    _setDioInterceptorList();

    final standardHeaders = await _getOptions(apiType);

    return _dio
        .get(
          path,
          queryParameters: queries,
          options: standardHeaders,
        )
        .then((value) => value)
        .catchError(_getDioException);
  }

  Future<Response<dynamic>> post(
    APIType apiType,
    String path,
    Map<String, dynamic> data, {
    Map<String, dynamic>? headers,
    Map<String, dynamic>? queryParams,
  }) async {
    _setDioInterceptorList();

    final standardHeaders = await _getOptions(apiType);
    if (headers != null) {
      standardHeaders.headers?.addAll(headers);
    }

    return _dio
        .post(
          path,
          data: data,
          options: standardHeaders,
          queryParameters: queryParams,
        )
        .then((value) => value)
        .catchError(_getDioException);
  }

  Future<Response<dynamic>> patch(
    APIType api,
    String path,
    Map<String, dynamic> data, {
    Map<String, dynamic>? headers,
    Map<String, dynamic>? queryParams,
  }) async {
    _setDioInterceptorList();

    final standardHeaders = await _getOptions(api);
    if (headers != null) {
      standardHeaders.headers?.addAll(headers);
    }

    return _dio
        .patch(
          path,
          data: data,
          options: standardHeaders,
          queryParameters: queryParams,
        )
        .then((value) => value)
        .catchError(_getDioException);
  }

  Future<Response<dynamic>> put(
    APIType apiType,
    String path,
    Map<String, dynamic> data, {
    Map<String, dynamic>? headers,
    Map<String, dynamic>? queryParams,
  }) async {
    _setDioInterceptorList();

    final standardHeaders = await _getOptions(apiType);
    if (headers != null) {
      standardHeaders.headers?.addAll(headers);
    }

    return _dio
        .put(
          path,
          data: data,
          options: standardHeaders,
        )
        .then((value) => value)
        .catchError(_getDioException);
  }

  Future<Response<dynamic>> delete(
    APIType apiType,
    String path, {
    Map<String, dynamic>? data,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? queryParams,
  }) async {
    _setDioInterceptorList();

    final standardHeaders = await _getOptions(apiType);
    if (headers != null) {
      standardHeaders.headers?.addAll(headers);
    }

    return _dio
        .delete(
          path,
          data: data,
          options: standardHeaders,
        )
        .then((value) => value)
        .catchError(_getDioException);
  }

  dynamic _getDioException(error) {
    if (error is DioError) {
      _dioError(error);
    } else {
      throw UnexpectedException(
        ErrorResponseModel(
          error: ErrorModel(
            code: "500",
            message: TextConstants.defaultErrorMessage,
          ),
        ),
      );
    }
  }

  void _dioError(DioError error) {
    Log.error(
      'DIO ERROR: ${error.type} ENDPOINT: ${error.requestOptions.baseUrl}${error.requestOptions.path}',
    );
    switch (error.type) {
      case DioErrorType.cancel:
        throw RequestCancelledException(
          ErrorResponseModel(
            error: ErrorModel(
              code: "499",
              message: TextConstants.requestCancelled,
            ),
          ),
        );
      case DioErrorType.connectionTimeout:
        ErrorResponseModel errorModel = ErrorResponseModel(
          error: ErrorModel(
            code: "408",
            message: TextConstants.couldNotConnectToServer,
          ),
        );

        throw RequestTimeoutException(errorModel);
      case DioErrorType.unknown:
        throw DefaultException(
          ErrorResponseModel(
            error: ErrorModel(
              code: '500',
              message: TextConstants.defaultErrorMessage,
            ),
          ),
        );
      case DioErrorType.receiveTimeout:
        throw ReceiveTimeoutException(
          ErrorResponseModel(
            error: ErrorModel(
              code: "408",
              message: TextConstants.couldNotConnectToServer,
            ),
          ),
        );
      case DioErrorType.sendTimeout:
        ErrorResponseModel errorModel = ErrorResponseModel(
          error: ErrorModel(
            code: "408",
            message: TextConstants.couldNotConnectToServer,
          ),
        );

        throw RequestTimeoutException(errorModel);
      case DioErrorType.badCertificate:
        throw BadCertificateException(
          ErrorResponseModel(
            error: ErrorModel(
              code: "500",
              message: TextConstants.defaultErrorMessage,
            ),
          ),
        );
      case DioErrorType.badResponse:
        throw BadResponseException(
          ErrorResponseModel(
            error: ErrorModel(
                code: error.response?.statusCode.toString() ?? "500",
                message: error.response?.data
                    .toString()
                    .replaceAll(RegExp('[{}]'), '')),
          ),
        );
      case DioErrorType.connectionError:
        ErrorResponseModel errorModel = ErrorResponseModel(
          error: ErrorModel(
            code: "500",
            message: TextConstants.defaultErrorMessage,
          ),
        );

        _networkException(
          errorModel,
          error.response?.statusCode,
          error.response?.statusMessage ?? '',
        );
        break;
      default:
        UnexpectedException(
          ErrorResponseModel(
            error: ErrorModel(
              code: "500",
              message: TextConstants.defaultErrorMessage,
            ),
          ),
        );
    }
  }

  void _networkException(
    ErrorResponseModel errorModel,
    int? statusCode,
    String message,
  ) {
    switch (statusCode) {
      case 400:
        throw BadRequestException(errorModel);
      case 403:
        throw UnauthorisedException(errorModel);
      case 401:
        throw UnauthorisedException(errorModel);
      case 404:
        throw NotFoundException(errorModel);
      case 409:
        throw ConflictException(errorModel);
      case 500:
        throw InternalServerException(errorModel);
      case 502:
        throw BadGatewayException(errorModel);
      default:
        throw DefaultException(
          ErrorResponseModel(
            error: ErrorModel(
              code: '500',
              message: TextConstants.defaultErrorMessage,
            ),
          ),
        );
    }
  }

  void _setDioInterceptorList() {
    List<Interceptor> interceptorList = [];
    _dio.interceptors.clear();

    if (kDebugMode) {
      interceptorList.add(PrettyDioLogger());
    }
    _dio.interceptors.addAll(interceptorList);
  }

  Future<Options> _getOptions(APIType api) async {
    String? token = await cacheService.read(CacheKeys.token);
    switch (api) {
      case APIType.open:
        return PublicApiOptions().options;

      case APIType.restricted:
        return ProtectedApiOptions(token!).options;

      default:
        return PublicApiOptions().options;
    }
  }
}

abstract class ApiOptions {
  Options options = Options();
}

enum APIType { open, restricted }

class PublicApiOptions extends ApiOptions {
  PublicApiOptions() {
    super.options.headers = <String, dynamic>{
      'Accept': 'application/json',
      'Content-type': 'application/json',
    };
  }
}

class ProtectedApiOptions extends ApiOptions {
  ProtectedApiOptions(String apiToken) {
    super.options.headers = <String, dynamic>{
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $apiToken',
    };
  }
}
