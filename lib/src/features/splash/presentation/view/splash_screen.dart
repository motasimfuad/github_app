import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:github_app/src/core/assets/assets.dart';
import 'package:github_app/src/core/navigation/routes.dart';
import 'package:github_app/src/core/services/cache/cache_keys.dart';
import 'package:github_app/src/core/services/cache/cache_service.dart';
import 'package:go_router/go_router.dart';

class SplashScreen extends ConsumerStatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends ConsumerState<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _goToNextRoute();
  }

  void _goToNextRoute() async {
    await ref.read(cacheServiceProvider).init();
    await Future.delayed(const Duration(seconds: 1));

    final cacheService = ref.read(cacheServiceProvider);

    final token = await cacheService.read(CacheKeys.token);

    // check if token is available
    if (mounted) {
      context.goNamed(Routes.home);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 120.h,
          child: Image.asset(
            Assets.logo,
          ),
        ),
      ),
    );
  }
}
