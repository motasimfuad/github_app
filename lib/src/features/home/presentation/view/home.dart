import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:github_app/src/core/assets/assets.dart';

class HomeScreen extends ConsumerWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Github App'),
      ),
      body: Center(
        child: SizedBox(
          height: 120.h,
          child: Image.asset(
            Assets.logo,
          ),
        ),
      ),
    );
  }
}
