import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:github_app/src/core/navigation/routes.dart';
import 'package:github_app/src/core/services/cache/cache_service.dart';
import 'package:github_app/src/core/style/theme.dart';
import 'package:github_app/src/features/home/presentation/view/home.dart';
import 'package:go_router/go_router.dart';

import 'src/features/splash/presentation/view/splash_screen.dart';

part 'src/core/navigation/router_configuration.dart';

class App extends ConsumerStatefulWidget {
  const App({super.key});

  @override
  ConsumerState<App> createState() => _RootState();
}

class _RootState extends ConsumerState<App> {
  @override
  void initState() {
    super.initState();
    initialize();
  }

  void initialize() async {
    await ref.read(cacheServiceProvider).init();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      useInheritedMediaQuery: true,
      designSize: const Size(375, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, _) {
        return MaterialApp.router(
          title: 'Github App',
          theme: themeData,
          debugShowCheckedModeBanner: false,
          routerConfig: _router,
          builder: (_, router) {
            return MediaQuery(
              data: MediaQuery.of(_).copyWith(
                textScaleFactor: 1.0,
              ),
              child: router!,
            );
          },
        );
      },
    );
  }
}
